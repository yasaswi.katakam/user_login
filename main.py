from flask import Flask

from scripts.service.login_service import blueprint_obj

app = Flask(__name__)
app.register_blueprint(blueprint_obj)

if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.run(port=9200)
