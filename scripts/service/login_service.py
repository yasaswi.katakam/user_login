from flask import Blueprint, render_template, request, flash
import bcrypt
from scripts.handler.login_handler import handler_class
blueprint_obj = Blueprint('blueprint_obj', __name__)
handler_class_obj = handler_class()


@blueprint_obj.route('/login')
def login_page():
    return render_template('login.html')


@blueprint_obj.route('/registration_page')
def registration_page():
    return render_template('registration.html')


@blueprint_obj.route('/collect_data', methods=['POST'])
def collect_data():
    flag = handler_class_obj.find_obj(request.form["username"], request.form["pwd"])
    if flag == 1:
        flash("Login Successfull")
        return render_template('home.html')
    else:
        error = "Please enter correct password or Username"
        return render_template('login.html', error=error)


@blueprint_obj.route('/register_data', methods=['POST'])
def insert_data():
    data = {"Firstname": request.form["firstname"],
            "Middlename": request.form["middlename"],
            "Lastname": request.form["lastname"],
            "Phone": request.form["phone"],
            "Username": request.form["username"],
            "Password": bcrypt.hashpw((request.form["password"]).encode('utf8'), bcrypt.gensalt())
            }
    flag = handler_class_obj.insert(data, request.form["username"])
    if flag == 1:
        flash("you have successfully registered.You can Login")
        return render_template('login.html')
    else:
        error = "User_id already exists"
        return render_template('registration.html', error=error)
