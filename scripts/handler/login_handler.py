import pymongo
from pymongo import MongoClient

myclient = pymongo.MongoClient("mongodb://localhost:27017")
mydb = myclient["yasaswi"]
register_data = mydb["register_data"]


class handler_class:

    @staticmethod
    def find_obj(username, password):
        flag = 0
        details = list(register_data.find({"User Id": username}))
        if len(details) != 0:
            find = register_data.find({"User Id": username})
            for key, value in find.next().items():
                if (key == "Password" and value == password):
                    flag = 1
                    break
        return flag

    @staticmethod
    def insert(data, username):
        details1 = register_data.find({"Username": username})
        flag = 0
        if len(list(details1)) == 0:
            register_data.insert(data)
            flag = 1
        return flag

